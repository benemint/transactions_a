#Begin
CalculateDescriptives = function(x){ 
  
  Year = as.integer(format(Sys.time(), "%Y"))-2 #Gives current year - 2 based on system time, here objective is year 2015
  perioids = length(unique(BD$Months[which(BD$Years==Year)])) #Gives available months in that year 
  
  MonthlyIncome <- sum(x[which(x>0&BD$Years==Year)])/perioids
  MonthlyExpenditure <- sum(x[which(x<0&BD$Years==Year)])/perioids
  NetMonthlySum <- MonthlyIncome + MonthlyExpenditure
  YearlyIncome = MonthlyIncome*12 # Estimated full year income for the perioid
  YearlyExpenditure = MonthlyExpenditure*12 # same but for expenditure
  
  #This Builds a dataframe "Results" with descriptives if NetMonthlySum is positive = (person can save)
  if (NetMonthlySum > 0) {
    Results = data.frame(MonthlyIncome,MonthlyExpenditure,NetMonthlySum,YearlyIncome,YearlyExpenditure, Year, perioids)
    print(Results)
  }
}

#Function to calculate Descriptives by Year
#Begin
CalculateDescriptivesYear = function(x,y){ 
  Year <- y
  perioids <- length(unique(BD$Months[which(BD$Years==Year)])) #Gives available months in that year 
  
  MonthlyIncome <- sum(x[which(x>0&BD$Years==Year)])/perioids
  MonthlyExpenditure <- sum(x[which(x<0&BD$Years==Year)])/perioids
  NetMonthlySum <- MonthlyIncome + MonthlyExpenditure
  YearlyIncome <- MonthlyIncome*12 # Estimated full year income for the perioids
  YearlyExpenditure <- MonthlyExpenditure*12 # same but for expenditure
  
  #This Builds a dataframe "Results" with the above information if "NetMonthlySum" is positive = (will save) IF part commented out
  #if (NetMonthlySum > 0) {
  Results = data.frame(MonthlyIncome,MonthlyExpenditure,NetMonthlySum,YearlyIncome,YearlyExpenditure, Year, perioids)
  print(Results)
  #}
}

AllDescriptives = function(x){ 
  for(i in unique(BD$Years)) {
    CalculateDescriptivesYear(Value,i)  
  }
} 

OptimalSavingsAmmount = function(x) {
  BD2 <- (BD) %>% 
    group_by(Months, Years) %>%
    summarise(transactions = sum(transactions)) %>%
    mutate(NetMonthlySum = cumsum(transactions))
  
  #Plot for checking plausibility
  plot(BD2$NetMonthlySum)
  abline(h=mean(BD2$NetMonthlySum))
  abline(h=mean(BD2$NetMonthlySum)+sd(BD2$NetMonthlySum), col="red")
  abline(h=mean(BD2$NetMonthlySum)-sd(BD2$NetMonthlySum), col="red")
  
  #Table of values for each year for comparison
  Quantiles = function(x){
    Year <- x
    Presentiles = quantile(BD2$NetMonthlySum[which(BD2$Years==Year)])
    print("Monthly Net Sums as quantiles on year of")
    print(Year)
    print(Presentiles)
  }
  
  for(i in unique(BD2$Years)) {
    Quantiles(i)  
  }
  
  AllPerioid = quantile(BD2$NetMonthlySum)
  MinBalance = quantile(BD2$NetMonthlySum, probs = 1, names=FALSE)
  print("For whole perioid of data")
  print(AllPerioid)
  #Actual suggestions for user
  print("Suggested savings amount is")
  print(round(mean(BD2$NetMonthlySum)*0.5, digits=-1))
  print("You should have in your account at least")
  print(round(MinBalance, digits=-2))
  
}