##################################################
#Bank Data forecast
##################################################

#Next time, parameters could be optimized in more complex models?
# Clean the excessive functions to source file? or in a neater way? --> In progress
# Test if cumulative montly sums is better than cumulative sums
#Answer: It is approximitely 10 % better
# How about individual smoothers per each month? Look at the Example in HighStat
# How about weighted monthly means?
#example
#wt <- c(5,  5,  4,  1)/15
#x <- c(3.7,3.3,3.5,2.8)
#xm <- weighted.mean(x, wt)


rm(list=ls())
gc()

setwd("J:/R") #define working directory
.libPaths() # this helps with library version conflicts

#Libraries needed
library(mgcv)
library(car)
library(ggplot2)
#library(dtplyr) not needed in current version
library(data.table)
library(grid)


#Just a memorylist dont run me
# detach("package:data.table", unload=TRUE)
# library(dtplyr) 
# makes these work together:
# library(data.table)
# library(dplyr)

#####################################################################
# 1. Data Preparation
#####################################################################
# Expected input data
# csv file, ";" separated
# desimal separator ","
# 2 colums 
# "sdate" type: character, format: "1.1.2015"
# "transactions" type: float/number, "-0,1" 

# Make data in //data in working directory
BD <- read.csv(file = "data//transactions.csv",
               header = TRUE,
               sep=";",
               dec = ",")
colnames(BD) <- c("sdate", "transactions")  # Forcing names

BD$Dates <- as.Date(BD$sdate,
                       format = "%d.%m.%Y")
#Check formating is correct
sum(is.na(BD$Dates))
# 0 is good

#Data is set so that week starts from Monday
# Generating NA's for new data
namevector <- c("Weekdays", "Daynum", "Weeks", "Months", "Years") 
BD[,namevector] <- NA
#Getting the weekdays
dow <- function(x) format(as.Date(x), "%u")
BD$Weekdays <- as.integer(dow(BD$Dates))
#Getting the day of the month
dodm <- function(x) format(as.Date(x), "%d")
BD$Daynum <- as.integer(dodm(BD$Dates))
#Getting the weeks
dowe <- function(x) format(as.Date(x), "%W")
BD$Weeks <- as.integer(dowe(BD$Dates))
#Getting the months
dom <- function(x) format(as.Date(x), "%m")
BD$Months <- as.integer(dom(BD$Dates))
#Getting the years
doy <- function(x) format(as.Date(x), "%Y")
BD$Years <- as.integer(doy(BD$Dates))

#####################################################################
# 2. Descriptives
#####################################################################
# Calculating yearly and monthly income based on last year
# Needs data preparation run first

#####################################################################
#Function to calculate Descriptives for system time year - 2 
# OBSOLETE
 
#Begin
CalculateDescriptives = function(x){ 

Year = as.integer(format(Sys.time(), "%Y"))-2 #Gives current year - 2 based on system time, here objective is year 2015
perioids = length(unique(BD$Months[which(BD$Years==Year)])) #Gives available months in that year 

MonthlyIncome <- sum(x[which(x>0&BD$Years==Year)])/perioids
MonthlyExpenditure <- sum(x[which(x<0&BD$Years==Year)])/perioids
NetMonthlySum <- MonthlyIncome + MonthlyExpenditure
YearlyIncome = MonthlyIncome*12 # Estimated full year income for the perioid
YearlyExpenditure = MonthlyExpenditure*12 # same but for expenditure

#This Builds a dataframe "Results" with descriptives if NetMonthlySum is positive = (person can save)
if (NetMonthlySum > 0) {
  Results = data.frame(MonthlyIncome,MonthlyExpenditure,NetMonthlySum,YearlyIncome,YearlyExpenditure, Year, perioids)
  print(Results)
}
}
#####################################################################
#End
#Test function CalculateDescriptives
Value = BD$transactions # 1. argument needs to refer to the database
CalculateDescriptives(Value)

#####################################################################
#Function to calculate Descriptives by Year
#Begin
CalculateDescriptivesYear = function(x,y){ 
  Year <- y
  perioids <- length(unique(BD$Months[which(BD$Years==Year)])) #Gives available months in that year 
  
  MonthlyIncome <- sum(x[which(x>0&BD$Years==Year)])/perioids
  MonthlyExpenditure <- sum(x[which(x<0&BD$Years==Year)])/perioids
  NetMonthlySum <- MonthlyIncome + MonthlyExpenditure
  YearlyIncome <- MonthlyIncome*12 # Estimated full year income for the perioids
  YearlyExpenditure <- MonthlyExpenditure*12 # same but for expenditure
  
  #This Builds a dataframe "Results" with the above information if "NetMonthlySum" is positive = (will save) IF part commented out
  #if (NetMonthlySum > 0) {
    Results = data.frame(MonthlyIncome,MonthlyExpenditure,NetMonthlySum,YearlyIncome,YearlyExpenditure, Year, perioids)
    print(Results)
  #}
}
#####################################################################
#End
#Test function CalculateDescriptives
#Value = BD$transactions
# First argument is the transactions value in the database
# Second argument is the year you want the descriptives
#CalculateDescriptivesYear(Value, 2015)

#This calculates all descriptives for all available years
Value = BD$transactions # 1. argument needs to refer to the database transactions

AllDescriptives = #function(x){ 
  for(i in unique(BD$Years)) {
CalculateDescriptivesYear(Value,i)  
}
#}

#Run if function by:
#AllDescriptives()

#Generation of ID (not in use)
# ID <- seq(1,nrow(BD),by=1)
# BD$ID <- ID
#####################################################################

#OnlyMonthlyValues require(dplyr)
detach("package:data.table", unload=TRUE) #Conflict between library
library(dplyr)
# This is to calculate safe savings sum
#Begin
OptimalSavingsAmmount = function(x) {
BD2 <- (BD) %>% 
  group_by(Months, Years) %>%
  summarise(transactions = sum(transactions)) %>%
  mutate(NetMonthlySum = cumsum(transactions))
  
  #Plot for checking plausibility
  plot(BD2$NetMonthlySum)
  abline(h=mean(BD2$NetMonthlySum))
  abline(h=mean(BD2$NetMonthlySum)+sd(BD2$NetMonthlySum), col="red")
  abline(h=mean(BD2$NetMonthlySum)-sd(BD2$NetMonthlySum), col="red")
  
  #Table of values for each year for comparison
    Quantiles = function(x){
        Year <- x
        Presentiles = quantile(BD2$NetMonthlySum[which(BD2$Years==Year)])
        print("Monthly Net Sums as quantiles on year of")
        print(Year)
        print(Presentiles)
    }

    for(i in unique(BD2$Years)) {
      Quantiles(i)  
    }
    
    AllPerioid = quantile(BD2$NetMonthlySum)
    MinBalance = quantile(BD2$NetMonthlySum, probs = 1, names=FALSE)
    print("For whole perioid of data")
    print(AllPerioid)
    #Actual suggestions for user
    print("Suggested savings amount is")
    print(round(mean(BD2$NetMonthlySum)*0.5, digits=-1))
    print("You should have in your account at least")
    print(round(MinBalance, digits=-2))
          
}
OptimalSavingsAmmount()
#End
#####################################################################
# 3. Data Analysis
#####################################################################

#Cumulative values, simple solution
BD$Cumt <- cumsum(BD$transactions)


#After running this, the descriptive stats won't work, because of library conflict.
#Cumulative sums by months (restarts at each month) require(data.table)
detach("package:dplyr", unload=TRUE)
library(data.table)
BD <- data.table(BD)
BD[, Cum.Sum := cumsum(transactions), by=list(Months)]

#to show the difference of alternative cumulative sums
plot(BD$Cum.Sum, type = "l")
lines(BD$Cumt, col="red")

#For now, we mask the new Cum.Sum as Cumt to be able to run old functions with no changes to commands
BD$Cumt = BD$Cum.Sum


attach(BD)
plot(Dates, Cumt, type="l")


head(BD)
str(BD) #check that Numbers, Dates and Integes, no factors




ggplot(BD, aes(Dates, Cumt)) +
  geom_line() +
  theme(panel.border = element_blank(), panel.background = element_blank(), panel.grid.minor = element_line(colour = "grey90"),
        panel.grid.major = element_line(colour = "grey90"), panel.grid.major.x = element_line(colour = "grey90"),
        axis.text = element_text(size = 10),
        axis.title = element_text(size = 12, face = "bold")) +
  labs(x = "Date", y = "Transactions")

names(BD)
# [1] "sdate"        "transactions" "Dates"        "Weekdays"     "Months"      
# [6] "Years"        "Cumt"         "Weeks"        "Daynum" 

gam_1 <- gam(Cumt ~ s(Weekdays, Months),
             data = BD,
             family = gaussian)

Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gam_1$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)


ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAM n.1")


gam_2 <- gam(Cumt ~ te(Daynum, Months,
                       bs = c("cr", "ps")) + Years,
             data = BD,
             family = gaussian)

gamma <- gam(Cumt ~s(Daynum, Months) + Years, 
             data=BD,
             family=gaussian)

AIC(gam_1, gam_2, gamma)
#Gamma visualization
Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gamma$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)


ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAMma")

#gam_2 visualization
Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gam_2$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)


ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAM 2")

gam_4 <- gam(Cumt ~ te(Daynum, Months,
                       k = c(7, 12),
                       bs = c("cr", "ps")) + Years,
             data = BD,
             family = gaussian)

plot(gam_4, se = FALSE, rug = FALSE)
summary(gam_4)

gam_5 <- gam(Cumt ~ s(Daynum, bs = "cr", k = 7) +
               s(Months, bs = "ps", k = 12) +
               ti(Daynum, Months,
                  k = c(7, 12),
                  bs = c("cr", "ps")),
             data = BD,
             family = gaussian)

AIC(gam_1, gam_2, gamma, gam_4, gam_5)
plot(gam_5,pages=1)
gam.check(gam_5,pch=19,cex=.3)
qq.gam(gam_5)

gam_6 <- gam(Cumt ~ t2(Daynum, Months,
                       k = c(31, 12),
                       bs = c("cr", "ps"),
                       full = TRUE),
             data = BD,
             family = gaussian)
gam_6y <- gam(Cumt ~ t2(Daynum, Months,
                       k = c(31, 12),
                       bs = c("cr", "ps"),
                       full = TRUE) + Years,
             data = BD,
             family = gaussian)

gam_6v <- gam(Cumt ~ t2(Daynum,
                       k = c(10),
                       bs = c("cr"),
                       full = TRUE),
                       data = BD,
                       family = gaussian)
gam_6byM <- gam(Cumt ~ t2(Daynum, by = Months,
                          k = 31),
              data = BD,
              family = gaussian)

AIC(gam_6,gam_6y)

#This is with the old dataset
# Family: gaussian 
# Link function: identity 
# 
# Formula:
#   Cumt ~ t2(Daynum, Months, k = c(7, 12), bs = c("cr", "ps"), full = TRUE) + 
#   Years
# 
# Parametric coefficients:
#   Estimate Std. Error t value Pr(>|t|)    
# (Intercept) -1.648e+06  7.842e+04  -21.01   <2e-16 ***
#   Years        8.185e+02  3.891e+01   21.04   <2e-16 ***
#   ---
#   Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
# 
# Approximate significance of smooth terms:
#   edf Ref.df     F p-value    
# t2(Daynum,Months) 50.21  54.92 23.46  <2e-16 ***
#   ---
#   Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
# 
# R-sq.(adj) =  0.705   Deviance explained = 71.9%
# GCV = 2.8242e+05  Scale est. = 2.6867e+05  n = 1072

max(BD$Cumt)

AIC(gam_6y, gam_6)

AIC(gam_4, gam_5, gam_6)
#gam_6y best
layout(matrix(1:2, nrow = 1))
plot(gam_4, rug = FALSE, se = FALSE, n2 = 80, main = "gam n.4 with te()")
plot(gam_6, rug = FALSE, se = FALSE, n2 = 80, main = "gam n.6 with t2()")
dev.off()

#3D
vis.gam(gam_6byM, view = c("Daynum", "Months"), n.grid = 100, theta = 35, phi = 32, zlab = "",
        ticktype = "detailed", color = "topo", main = "t2(D, W)")

vis.gam(gam_6byM,theta=-35,color="heat",cond=list(z=1))
vis.gam(gam_6byM,view=c("Months","Daynum"),theta= -135) # plot against by variabl

#2D
vis.gam(gam_6byM, main = "t2(D, W)", plot.type = "contour",
        color = "terrain", contour.col = "black", lwd = 2)

# This is how avearge months balance looks like
plot(gam_6v)
points(BD$Daynum, BD$Cumt, lwd=0.1)

# Look at autocorrelation of residuals

layout(matrix(1:2, ncol = 2))
acf(resid(gam_6y), lag.max = 48, main = "ACF")
pacf(resid(gam_6y), lag.max = 48, main = "pACF")

# within-group correlation structure

gam_6_ar0 <- gamm(Cumt ~ t2(Daynum, Months,
                            k = c(31, 12),
                            bs = c("cr", "ps"),
                            full = TRUE),
                  data = BD,
                  family = gaussian,
                  method = "REML")

gam_6_ar1 <- gamm(Cumt ~ t2(Daynum, Months,
                            k = c(31, 12),
                            bs = c("cr", "ps"),
                            full = TRUE),
                  data = BD,
                  family = gaussian,
                  correlation = corARMA(form = ~ 1|Daynum, p = 1),
                  method = "REML")

library(forecast)

arima_1 <- auto.arima(resid(gam_6_ar0$lme, type = "normalized"),
                      stationary = TRUE, seasonal = FALSE)
summary(arima_1)
arima_1$model$theta
arima_1$model$phi
arima_1$coef

gam_6_arma <- gamm(Cumt ~ t2(Daynum, Months,
                             k = c(31, 12),
                             bs = c("cr", "ps"),
                             full = TRUE) + Years,
                   data = BD,
                   family = gaussian,
                   correlation = corARMA(form = ~ 1|Daynum, p = 1, q = 2),
                   method = "REML")

layout(matrix(1:2, ncol = 2))
acf(resid(gam_6_ar1$lme, type = "normalized"), lag.max = 31, main = "ACF")
pacf(resid(gam_6_ar1$lme, type = "normalized"), lag.max = 31, main = "pACF")
dev.off()

layout(matrix(1:2, ncol = 2))
acf(resid(gam_6_arma$lme, type = "normalized"), lag.max = 31, main = "ACF")
pacf(resid(gam_6_arma$lme, type = "normalized"), lag.max = 31, main = "pACF")
dev.off()

anova(gam_6_ar0$lme, gam_6_ar1$lme, gam_6_arma$lme)
anova(gam_6_ar0$lme, gam_6_ar1$lme)
# gam_6 with AR(1) correlated errors seems better than simple model, ARMA is slightly better than AR

summary(gam_6_arma$gam)
summary(gam_6_ar1$gam)
summary(gam_6_ar0$gam)
summary(gam_6y)

# intervals(gam_6_ar1$lme, which = "var-cov")$corStruct
# intervals(gam_6_arma$lme, which = "var-cov")$corStruct

#Gamm Arma fit
Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gam_6_arma$gam$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)

ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAM Arma")

#Gam 6 fit
Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gam_6$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)

ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAM 6y fit")
summary(gam_6y)

##################################################################################
# residuals
##################################################################################

datas <- data.table(Fitted_values = c(gam_6_ar0$gam$fitted.values, gam_6_arma$gam$fitted.values),
                    Residuals = c(gam_6_ar0$gam$residuals, gam_6_arma$gam$residuals),
                    Model = rep(c("Gam n.6", "Gam n.6 with ARMA)"), each = nrow(BD)))


ggplot(data = datas,
       aes(Fitted_values, Residuals)) +
  facet_grid(Model~., switch = "y") +
  geom_point(size = 1.7) +
  geom_smooth(method = "loess") +
  geom_hline(yintercept = 0, color = "red", size = 1) +
  theme(panel.border = element_rect(fill = NA, color = "black"),
        plot.title = element_text(size = 14, hjust = 0.5, face = "bold"),
        axis.text = element_text(size = 11),
        axis.title = element_text(size = 12, face = "bold"),
        strip.text = element_text(size = 13, face = "bold"),
        strip.background = element_rect(color = "black")) +
  labs(title = "Fitted values vs Residuals of two models")


vis.gam(gam_6, n.grid = 50, theta = 35, phi = 32, zlab = "",
        ticktype = "detailed", color = "topo", main = "t2(D, W)")
# 2D - topological view (contour lines)
vis.gam(gam_6, main = "t2(D, W)", plot.type = "contour",
        color = "terrain", contour.col = "black", lwd = 2)
dev.off()
# plot option does very similar job Daily and Weekly
plot(gam_6_arma$gam, rug=FALSE, se = F, n2 = 80)
##################################################################################
## Evaluation of time complexity ----
##################################################################################
library(microbenchmark)

#this takes at least minutes

res1 <- microbenchmark(GAM = gam(Cumt ~ t2(Daynum, Months,
                                           k = c(31, 12),
                                           bs = c("cr", "ps"),
                                           full = TRUE) + Years ,
                                 data = BD,
                                 family = gaussian),
                       MLR = lm(Cumt ~ 0 + Daynum + Months + Daynum:Months, data = BD),
                       times = 3)

plot(res1)
autoplot(res1)


#Just linear for comparison, it is pretty much insensible
BDlm = lm(Cumt ~Daynum + Months + Daynum:Months, data = BD)
plot(BDlm)
plot(Cumt ~ Daynum, data = BD, xlab = "Days", ylab = "Value")
coef(BDlm)


##################################################################################
# Models to predict: gam_6,gam_6y
# Predictive tests
##################################################################################
#Full Model

#Gam 6 fit
Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gam_6$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)

ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAM 6y fit")
summary(gam_6y)

#structure again

str(BD)
min(BD$Dates)
max(BD$Dates)
quantile(as.integer(BD$Dates))

#PREDICTIONS - DATA SUBSET 1 YEAR TRAIN

BDcut <- subset(BD, BD$Dates > "2015-06-12" )  #Cut dataset
BDteach <- subset(BD, BD$Dates < "2016-06-12" & BD$Dates > "2015-06-12" )  #teach dataset
BDtest <- subset(BD, BD$Dates > "2016-06-12")   #test dataset
str(BDtest)
#MODELS
gam_6yS <- gam(Cumt ~ t2(Daynum, Months,
                        k = c(15, 12),
                        bs = c("cr", "ps"),
                        full = TRUE) + Years,
              data = BDteach,
              family = gaussian)

gam_6S <- gam(Cumt ~ t2(Daynum, Months,
                         k = c(5, 12), #changing the Daynum K is good for controlling overfitting
                         bs = c("cr", "ps"),
                         full = TRUE),
               data = BDteach,
               family = gaussian)


AIC(gam_6yS, gam_6S)


#PREDICTIONS
gam_6ySp <-  predict(gam_6yS, BDtest, se=TRUE)
gam_6ySp1 <-  predict(gam_6yS, BDtest,type="terms",se=TRUE)
gam_6ySp2 <-  predict(gam_6yS,  BDtest,type="iterms",se=TRUE)
#2nd model
gam_6Sp1 <-  predict(gam_6S, BDtest ,type="terms",se=TRUE)

#Model 1. gam_6S with gam_6Sp1 predictions
Datas1 = data.table(Cumt = BDcut$Cumt, data_time=BDcut$Dates, group=rep("Real", nrow(BDcut)))
Datas2 = data.table(Cumt = gam_6Sp1$fit[,1], data_time = BDtest$Dates, group=rep("Predicted", nrow(BDtest)))
Datas3 = data.table(Cumt = gam_6S$fitted.values, data_time = BDteach$Dates, group=rep("Fitted", nrow(BDteach)))
p <- ggplot() + 
  geom_line(aes(data_time, Cumt, colour=group), Datas1) +  
  geom_line(aes(data_time, Cumt, colour=group), Datas2) +
  geom_line(aes(data_time, Cumt, colour=group), Datas3) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Predictions from model gam_6S ")
  
  
#quick visualization
plot(BDtest$Cumt, type="l")
lines(gam_6ySp$fit, col="red", type="l")
lines(gam_6ySp1$fit[,2], col="green")
lines(gam_6ySp2$fit[,2], col="blue")

mean(gam_6ySp$se.fit) #mean standard error for all predictions
mean(gam_6ySp1$se.fit)
mean(gam_6ySp2$se.fit)



# Example for calculating predictions and MRSE

n<-200
sig <- 2
dat <- gamSim(1,n=n,scale=sig)

b<-gam(y~s(x0)+s(I(x1^2))+s(x2)+offset(x3),data=dat)

newd <- data.frame(x0=(0:30)/30,x1=(0:30)/30,x2=(0:30)/30,x3=(0:30)/30)
pred <- predict.gam(b,newd)
pred0 <- predict(b,newd,exclude="s(x0)") ## prediction excluding a term


Xp <- predict(b,newd,type="lpmatrix") 

## Xp %*% coef(b) yields vector of predictions

a <- rep(1,31)
Xs <- t(a) %*% Xp ## Xs %*% coef(b) gives sum of predictions
var.sum <- Xs %*% b$Vp %*% t(Xs)


#############################################################
## Now get the variance of non-linear function of predictions
## by simulation from posterior distribution of the params
#############################################################

rmvn <- function(n,mu,sig) { ## MVN random deviates
  L <- mroot(sig);m <- ncol(L);
  t(mu + L%*%matrix(rnorm(m*n),m,n)) 
}

br <- rmvn(1000,coef(b),b$Vp) ## 1000 replicate param. vectors
res <- rep(0,1000)
for (i in 1:1000)
{ pr <- Xp %*% br[i,] ## replicate predictions
res[i] <- sum(log(abs(pr))) ## example non-linear function
}
mean(res);var(res)

## loop is replace-able by following .... 

res <- colSums(log(abs(Xp %*% t(br))))

##################################################################
# illustration of unsafe scale dependent transforms in smooths....
##################################################################

b0 <- gam(y~s(x0)+s(x1)+s(x2)+x3,data=dat) ## safe
b1 <- gam(y~s(x0)+s(I(x1/2))+s(x2)+scale(x3),data=dat) ## safe
b2 <- gam(y~s(x0)+s(scale(x1))+s(x2)+scale(x3),data=dat) ## unsafe
pd <- dat; pd$x1 <- pd$x1/2; pd$x3 <- pd$x3/2
par(mfrow=c(1,2))
plot(predict(b0,pd),predict(b1,pd),main="b0 and b1 predictions match")
abline(0,1,col=2)
plot(predict(b0,pd),predict(b2,pd),main="b2 unsafe, doesn't match")
abline(0,1,col=2)


##################################################################
## The following shows how to use use an "lpmatrix" as a lookup 
## table for approximate prediction. The idea is to create 
## approximate prediction matrix rows by appropriate linear 
## interpolation of an existing prediction matrix. The additivity 
## of a GAM makes this possible. 
## There is no reason to ever do this in R, but the following 
## code provides a useful template for predicting from a fitted 
## gam *outside* R: all that is needed is the coefficient vector 
## and the prediction matrix. Use larger `Xp'/ smaller `dx' and/or 
## higher order interpolation for higher accuracy.  
###################################################################

xn <- c(.341,.122,.476,.981) ## want prediction at these values
x0 <- 1         ## intercept column
dx <- 1/30      ## covariate spacing in `newd'
for (j in 0:2) { ## loop through smooth terms
  cols <- 1+j*9 +1:9      ## relevant cols of Xp
  i <- floor(xn[j+1]*30)  ## find relevant rows of Xp
  w1 <- (xn[j+1]-i*dx)/dx ## interpolation weights
  ## find approx. predict matrix row portion, by interpolation
  x0 <- c(x0,Xp[i+2,cols]*w1 + Xp[i+1,cols]*(1-w1))
}
dim(x0)<-c(1,28) 
fv <- x0%*%coef(b) + xn[4];fv    ## evaluate and add offset
se <- sqrt(x0%*%b$Vp%*%t(x0));se ## get standard error
## compare to normal prediction
predict(b,newdata=data.frame(x0=xn[1],x1=xn[2],
                             x2=xn[3],x3=xn[4]),se=TRUE)

####################################################################
## Differentiating the smooths in a model (with CIs for derivatives)
####################################################################

## simulate data and fit model...
dat <- gamSim(1,n=300,scale=sig)
b<-gam(y~s(x0)+s(x1)+s(x2)+s(x3),data=dat)
plot(b,pages=1)

## now evaluate derivatives of smooths with associated standard 
## errors, by finite differencing...
x.mesh <- seq(0,1,length=200) ## where to evaluate derivatives
newd <- data.frame(x0 = x.mesh,x1 = x.mesh, x2=x.mesh,x3=x.mesh)
X0 <- predict(b,newd,type="lpmatrix") 

eps <- 1e-7 ## finite difference interval
x.mesh <- x.mesh + eps ## shift the evaluation mesh
newd <- data.frame(x0 = x.mesh,x1 = x.mesh, x2=x.mesh,x3=x.mesh)
X1 <- predict(b,newd,type="lpmatrix")

Xp <- (X1-X0)/eps ## maps coefficients to (fd approx.) derivatives
colnames(Xp)      ## can check which cols relate to which smooth

par(mfrow=c(2,2))
for (i in 1:4) {  ## plot derivatives and corresponding CIs
  Xi <- Xp*0 
  Xi[,(i-1)*9+1:9+1] <- Xp[,(i-1)*9+1:9+1] ## Xi%*%coef(b) = smooth deriv i
  df <- Xi%*%coef(b)              ## ith smooth derivative 
  df.sd <- rowSums(Xi%*%b$Vp*Xi)^.5 ## cheap diag(Xi%*%b$Vp%*%t(Xi))^.5
  plot(x.mesh,df,type="l",ylim=range(c(df+2*df.sd,df-2*df.sd)))
  lines(x.mesh,df+2*df.sd,lty=2);lines(x.mesh,df-2*df.sd,lty=2)
}
