<<<<<<< HEAD
#BankDataAnalysis_short
=======
#BankDataAanalysis_short
>>>>>>> ef85063cfc39041c6285952876ff19e558e794e0

##################################################
#Bank Data forecast - short version
##################################################

rm(list=ls()) # empty working space
.libPaths() # this helps with library version conflicts


##################################################
#Libraries
library(mgcv)
library(car)
library(ggplot2)
library(data.table)
library(grid)
source("DescriptiveAnalytics.r")

##################################################
# 1. Data Preparation
##################################################

#Load data
BD <- read.csv(file = "data//transactions.csv",
               header = TRUE,
               sep=";",
               dec = ",")
colnames(BD) <- c("sdate", "transactions")  # Forcing names

BD$Dates <- as.Date(BD$sdate,
                    format = "%d.%m.%Y")


#Data is set so that week starts from Monday
# Generating NA's for new data
namevector <- c("Weekdays", "Daynum", "Weeks", "Months", "Years") 
BD[,namevector] <- NA
#Getting the weekdays
dow <- function(x) format(as.Date(x), "%u")
BD$Weekdays <- as.integer(dow(BD$Dates))
#Getting the day of the month
dodm <- function(x) format(as.Date(x), "%d")
BD$Daynum <- as.integer(dodm(BD$Dates))
#Getting the weeks
dowe <- function(x) format(as.Date(x), "%W")
BD$Weeks <- as.integer(dowe(BD$Dates))
#Getting the months
dom <- function(x) format(as.Date(x), "%m")
BD$Months <- as.integer(dom(BD$Dates))
#Getting the years
doy <- function(x) format(as.Date(x), "%Y")
BD$Years <- as.integer(doy(BD$Dates))

##################################################
# 2. Descriptives
##################################################
#The functions are in source file: DescriptiveAnalytics.r

Value = BD$transactions # 1. argument needs to refer to the database
#Function to calculate Descriptives by Year
#Currently #Gives current year - 2 based on system time, here objective is year 2015
CalculateDescriptives(Value)

#Function to calculate Descriptives by Year, this is internal function
#input: object, year
#CalculateDescriptivesYear(Value,2016)

#This calculates all descriptives for all available years
#no input needed
AllDescriptives()

#OnlyMonthlyValues require(dplyr)
detach("package:data.table", unload=TRUE) #Conflict between library
library(dplyr)
OptimalSavingsAmmount()
<<<<<<< HEAD
#The picture shows the net monthly sums
#The black is the mean
#Red lines are the 95 % confidence interval
#In this case the consistency is bad, yet recommendation is in line with persons wealth
=======
>>>>>>> ef85063cfc39041c6285952876ff19e558e794e0

##################################################
# 3. Data Analysis
##################################################
#Monthly Cumulative Values
detach("package:dplyr", unload=TRUE)
library(data.table)
BD <- data.table(BD)
BD[, Cum.Sum := cumsum(transactions), by=list(Months)]
BD$Cumt = BD$Cum.Sum

#General additive models, this takes 30 sec
gam_6 <- gam(Cumt ~ t2(Daynum, Months,
                       k = c(31, 12),
                       bs = c("cr", "ps"),
                       full = TRUE),
             data = BD,
             family = gaussian)

summary(gam_6)

##################################################
# 3. Visualizations
##################################################

#Visualizations 3D
vis.gam(gam_6,view=c("Months","Daynum"),theta= -135) # plot against by variables
#2D
vis.gam(gam_6, main = "t2(D, W)", plot.type = "contour",
        color = "terrain", contour.col = "black", lwd = 2)
#Raw Data
ggplot(BD, aes(Dates, Cumt)) +
  geom_line() +
  theme(panel.border = element_blank(), panel.background = element_blank(), panel.grid.minor = element_line(colour = "grey90"),
        panel.grid.major = element_line(colour = "grey90"), panel.grid.major.x = element_line(colour = "grey90"),
        axis.text = element_text(size = 10),
        axis.title = element_text(size = 12, face = "bold")) +
  labs(x = "Date", y = "Transactions")


#Final Fit
Datas1 = data.table(Cumt = BD$Cumt, data_time=BD$Dates, group=rep("Real", nrow(BD)))
Datas2 = data.table(Cumt = gam_6$fitted.values, data_time = BD$Dates, group=rep("Fitted", nrow(BD)))
datas <- rbindlist(list(Datas1,Datas2), use.names = TRUE, fill = TRUE)

ggplot(data = datas, aes(data_time, Cumt, group = group, colour = group)) +
  geom_line(size = 0.8) +
  theme_bw() +
  labs(x = "Time", y = "Balance",
       title = "Fit from GAM 6 fit")

<<<<<<< HEAD
# Why negative values? It is because of the monthly cumulative sums. 
# If there is residual balance from months before, negative balance shows how much of that is used in each months
# This was the most accurate option for prediction as the data is stationary.
=======
>>>>>>> ef85063cfc39041c6285952876ff19e558e794e0
